# Wireless monitoring system in parking lot using VANET

# Project Title Here

Wireless monitoring system in parking lot using VANET

## Usage

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* ns-3 (v3.29)
* Homemade simulator(C++,Eclipse/CLion) 


### Installing

How to install my program
* Step 1: Download source code and put folder in
sample folder
/home/bake/source/ns-3.29/src/

sampleProtocolScenario.cc file
/home/bake/source/ns-3.29/src/scratch

* Step 2: Arrangement of source code of homemade simulator
Place Sensorgraph files in the directory referenced by Eclipse and CLion


## Running the Examples

1.Set the value of GodNumber from 10 to 90
2.At the command prompt,Execute ./waf --run scratch/sampleProtocolScenario
3."Determine the number of nodes." Is displayed, so determine the total number of parked vehicles
4.Check if routing is properly implemented in NetAnim
5.Confirm that netNodePos4 is created in /bake/source/ns-3.29
6.Change netNodePos4 to any name and store it in the misc folder in sensorgraph
7.Compile and run SensorGraph.cc
8.Check that the area can be visualized on another screen


## Author

* **Kenshiro Hirai


## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
